import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import Title from './Title';
import reportWebVitals from './reportWebVitals';
import FullName from "./FullName";
import Rhombus from "./Rhombus";
import Console from "./Console";
// import TickingClock from "./Clock";

ReactDOM.render(
  <React.StrictMode>
    <Title />
    <Rhombus />
    <Console />
    <FullName firstName="Alberto" lastName="Nessi"/>
    {/* <TickingClock /> */}
  </React.StrictMode>,
  document.getElementById('root')
);


const TickingClock = () => {
  const clock =  (
      <div>
          <p><b>What time is it? </b>{new Date().toTimeString()}</p>
          {/* <p>Duration of the session: {SessionDuration ++}</p> */}
      </div>
  );
  ReactDOM.render(clock, document.getElementById("root"));
}
setInterval(TickingClock, 1000);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
