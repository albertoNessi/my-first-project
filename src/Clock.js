import React from 'react';
import ReactDOM from 'react-dom';

const TickingClock = () => {
    const clock =  (
        <div>
            <p><b>What time is it? </b>{new Date().toTimeString()}</p>
            {/* <p>Duration of the session: {SessionDuration ++}</p> */}
        </div>
    );
    ReactDOM.render(clock, document.getElementById("root"));
}
setInterval(TickingClock, 1000);

export default TickingClock;