const FullName = ({ firstName, lastName }) => {
    return (
      <div>
        <p className="FullName">
          <b>Nome: </b>{firstName},
        </p>
        <p>
            <b>Cognome: </b> {lastName}
        </p>
      </div>
    );
  };
  
  export default FullName;